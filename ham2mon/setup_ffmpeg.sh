#!/bin/bash
AUDIOSOURCE="hw:1,1,0"
AUDIO_OPTS="-ar 8000 -f alsa -ac 1"
OUTPUT_HLS="-hls_time 1 -hls_list_size 10 -http_persistent 0 -start_number 1 -hls_playlist_type event"
cd /usr/share/nginx/cart_console/ham2mon_stream
rm playlist*
ffmpeg  -y $AUDIO_OPTS -i "$AUDIOSOURCE" $OUTPUT_HLS playlist.m3u8
